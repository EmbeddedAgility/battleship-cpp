#pragma once

#include "Letters.h"
#include<iostream>


namespace Battleship
{
  namespace GameController
  {
    namespace Contracts
    {
      class Position
      {
      public:
        Position(Letters column = Letters::A, int row = 0);
        Position(const Position &init);
        ~Position();
      
      public:
        Letters Column;
        int Row;
        bool isHit = false;

      public:
        Position &operator=(const Position &rhs);
        bool operator==(const Position &rhs) const;
        friend std::ostream& operator<<(std::ostream& out, Position pos);
        bool isOutOfBounds(int mapsize) const {
          if (Column > mapsize || Row > mapsize) {
            return true;
          }
          return false;
        }
      };
    }
  }
}


