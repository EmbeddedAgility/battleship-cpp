#pragma once

#include <string>
#include <list>

#include "Position.h"
#include <iostream>

#include "../Battleship.GameController.Lib/termcolor/include/termcolor/termcolor.hpp"
namespace Battleship
{
  namespace GameController
  {
    namespace Contracts
    {
      class Ship
      {
      public:
        Ship();
        Ship(std::string Name, int Size);
        ~Ship();

      public:
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        std::string Name;

        /// <summary>
        /// Gets or sets the positions.
        /// </summary>
        std::list<Position> Positions;

        /// <summary>
        /// Gets or sets the size.
        /// </summary>
        int Size;

        /// <summary>
        /// The add position.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        void SetPosition(bool horizontal, const Position &root, int length)
        {
          if (horizontal)
          {
            for (auto i = 0u; i < length; ++i)
            {
              Positions.insert(Positions.end(), Position(root.Column, root.Row + i));
            }
          }
          else
          {
            for (int i = 0; i < length; ++i)
            {
              Positions.insert(Positions.end(),
                               Position(Letters(root.Column + i), root.Row));
            }
          }
        }
        void AddPosition(std::string input);
        void AddPosition(const Position &input);
        bool IsSunk();

        bool isCollision(const std::list<Ship> &ships, bool printDebug = false)
        {
          for (const auto &s : ships)
          {
            if (&s == this)
              continue;
            if (isCollision(s, printDebug))
            {
              return true;
            }
          }
          return false;
        }

        bool isCollision(const Ship &other, bool printDebug = false)
        {
          for (const auto &p : Positions)
          {
            for (const auto &o : other.Positions)
            {
              if (p == o)
              {
                if (printDebug) {
                  std::cout << std::endl << termcolor::bold << termcolor::on_red << "Collision detected at " << p << termcolor::reset << std::endl;
                }
                return true;
              }
            }
          }
          return false;
        }

        bool isOutOfBounds(int mapsize) {
          for (const auto &p : Positions) {
            if (p.isOutOfBounds(mapsize)) {
              return true;
            }
          }
          return false;
        }

        uint Length()
        {
          if (Name == "Aircraft Carrier")
          {
            return 5;
          }
          if (Name == "Battleship")
          {
            return 4;
          }
          if (Name == "Submarine")
          {
            return 3;
          }
          if (Name == "Destroyer")
          {
            return 3;
          }
          if (Name == "Patrol Boat")
          {
            return 2;
          }
        }
      };
    } // namespace Contracts
  }   // namespace GameController
} // namespace Battleship
