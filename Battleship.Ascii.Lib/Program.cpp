#include "Program.h"

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <algorithm>
#include <sstream>
#include <random>
#include <thread>

#include "../Battleship.GameController.Lib/GameController.h"
#include "../Battleship.GameController.Lib/termcolor/include/termcolor/termcolor.hpp"

#pragma comment(lib, "winmm.lib")  // for MSV C++

using namespace Battleship::GameController;
using namespace Battleship::GameController::Contracts;

namespace Battleship {
namespace Ascii {

int random(int begin, int end) {
  std::random_device dev;
  std::mt19937 rng(dev());
  std::uniform_int_distribution<std::mt19937::result_type> dist6(begin, end);

  return dist6(rng);
}

template <typename Color, typename TextColor>
void printShot(Color color, const std::string &message, TextColor textColor) {
  cout << color << R"(                \         .  ./         )"
       << termcolor::reset << endl;
  cout << color << R"(              \      .:"";'.:..""   /   )"
       << termcolor::reset << endl;
  cout << color << R"(                  (M^^.^~~:.'"").       )"
       << termcolor::reset << endl;
  cout << color << R"(            -   (/  .    . . \ \)  -    )"
       << termcolor::reset << endl;
  cout << color << R"(               ((| :. ~ ^  :. .|))      )"
       << termcolor::reset << endl;
  cout << color << R"(            -   (\- |  \ /  |  /)  -    )"
       << termcolor::reset << endl;
  cout << color << R"(                 -\  \     /  /-        )"
       << termcolor::reset << endl;
  cout << color << R"(                   \  \   /  /          )"
       << termcolor::reset << endl;
  cout << termcolor::reset;
  cout << termcolor::bold << textColor;
  cout << message << endl;
  cout << termcolor::reset;
}

template <typename Color, typename TextColor>
void printMessageWithBorder(Color frameColor, const std::string &message,
                            TextColor textColor, int messageWidth,
                            char frameChar) {
  cout << endl;

  for (int i = 0; i < 3; ++i) {
    for (int j = 0; j < messageWidth; ++j) {
      if (i == 0 || i == 2) {
        cout << frameColor << frameChar << termcolor::reset;
      } else {
        int leftOver = (messageWidth - message.size()) / 2;

        for (int k = 0; k < leftOver; ++k) {
          cout << frameColor << frameChar << termcolor::reset;
        }

        cout << textColor << message << termcolor::reset;

        for (int k = 0; k < leftOver; ++k) {
          cout << frameColor << frameChar << termcolor::reset;
        }

        break;
      }
    }

    cout << endl;
  }

  cout << endl;
}

template <typename Color, typename TextColor>
void animateMessage(Color frameColor, const std::string &message,
                    TextColor textColor, int messageWidth, char frameChar) {
  cout << endl;
  std::vector<std::string> lines;
  lines.push_back(" __ __   ___   __ __      __    __  ____  ____   __ ");
  lines.push_back("|  |  | /    \\|  |  |    |  |__|  ||    ||    \\ |  |");
  lines.push_back("|  |  ||     ||  |  |    |  |  |  | |  | |  _  ||  |");
  lines.push_back("|  ~  ||  O  ||  |  |    |  |  |  | |  | |  |  ||__|");
  lines.push_back("|___, ||     ||  :  |    |  `  '  | |  | |  |  | __ ");
  lines.push_back("|     ||     ||     |     \\      /  |  | |  |  ||  |");
  lines.push_back("|____/  \\___/  \\__,_|      \\_/\\_/  |____||__|__||__|");

  for (int j = 0; j < 2; ++j) {
    for (int i = 0; i < 20; ++i) {
      printMessageWithBorder(termcolor::on_green, "Congratulations!You win.",
                             termcolor::white, 40, '=');
      for (const auto &line : lines) {
        std::cout << termcolor::bold << frameChar;
        std::cout << std::string(i, ' ') << frameColor << line
                  << termcolor::reset << std::endl;
      }
      std::this_thread::sleep_for(std::chrono::milliseconds{100});
      system("clear");
    }
    for (int i = 20; i > 0; --i) {
      printMessageWithBorder(termcolor::on_green, "Congratulations!You win.",
                             termcolor::white, 40, '=');
      for (const auto &line : lines) {
        std::cout << termcolor::bold << frameChar;
        std::cout << std::string(i, ' ') << frameColor << line
                  << termcolor::reset << std::endl;
      }
      std::this_thread::sleep_for(std::chrono::milliseconds{100});
      system("clear");
    }
  }

  cout << endl;
}

bool CheckIsGameOver(std::list<Ship> &myShips, std::list<Ship> &enemyShips) {
  bool iWin = true;
  bool iLose = true;
  for (auto &ship : myShips) {
    iLose = iLose && ship.IsSunk();
  }

  for (auto &ship : enemyShips) {
    iWin = iWin && ship.IsSunk();
  }

  return iWin || iLose;
}

Program::Program() {}

Program::~Program() {}

list<Ship> Program::myFleet;
list<Ship> Program::enemyFleet;

void Program::Main() {
  cout
      << R"(                                     |__                                       )"
      << endl;
  cout
      << R"(                                     | \ /                                     )"
      << endl;
  cout
      << R"(                                     ---                                       )"
      << endl;
  cout
      << R"(                                     / | [                                     )"
      << endl;
  cout
      << R"(                              !      | |||                                     )"
      << endl;
  cout
      << R"(                            _/|     _/|-++'                                    )"
      << endl;
  cout
      << R"(                        +  +--|    |--|--|_ |-                                 )"
      << endl;
  cout
      << R"(                     { /|__|  |/\__|  |--- |||__/                              )"
      << endl;
  cout
      << R"(                    +---------------___[}-_===_.'____                 /\       )"
      << endl;
  cout
      << R"(                ____`-' ||___-{]_| _[}-  |     |_[___\==--            \/    _  )"
      << endl;
  cout
      << R"( __..._____--==/___]_|__|_____________________________[___\==--____,------' .7 )"
      << endl;
  cout
      << R"(|                        Welcome to Battleship                         BB-61/  )"
      << endl;
  cout
      << R"( \_________________________________________________________________________|   )"
      << endl;
  cout << endl;
  cout << "\033[0m";

  InitializeGame();

  StartGame();
}

void Program::StartGame() {
  // Console::Clear();
  cout << R"(                  __     )" << endl;
  cout << R"(                 /  \    )" << endl;
  cout << R"(           .-.  |    |   )" << endl;
  cout << R"(   *    _.-'  \  \__/    )" << endl;
  cout << R"(    \.-'       \         )" << endl;
  cout << R"(   /          _/         )" << endl;
  cout << R"(  |      _  /""          )" << endl;
  cout << R"(  |     /_\'             )" << endl;
  cout << R"(   \    \_/              )" << endl;
  cout << R"(    """"""""             )" << endl;

  do {
    cout << endl;
    cout << termcolor::on_green << R"(============== YOUR TURN ===============)"
         << termcolor::reset << endl
         << endl;

    bool PositionValid = false;
    string input;
    Position position;

    cout << R"(Enter coordinates for your shot :   (e.g. A3, B5, etc.))"
         << endl;
    getline(cin, input);

    if (input == "iwin") {
      animateMessage(termcolor::on_green, "Congratulations!You win.",
                     termcolor::white, 40, '=');
      break;
    } else if (input == "ilose") {
      printMessageWithBorder(termcolor::on_red, "You lose! :(",
                             termcolor::white, 40, '=');
      break;
    }

    position = ParsePosition(input);

    bool isHit =
        GameController::GameController::CheckIsHit(enemyFleet, position);
    if (isHit) {
      // Console::Beep();
      printShot(termcolor::on_red, "Yeah ! Nice hit !", termcolor::red);
    } else {
      printShot(termcolor::on_blue, "Miss! ", termcolor::blue);
    }

    if (CheckIsGameOver(myFleet, enemyFleet)) {
      printMessageWithBorder(termcolor::on_green, "Congratulations!You win.",
                             termcolor::white, 40, '=');
      break;
    }

    cout << endl
         << termcolor::on_red << R"(============= ENEMY'S TURN =============)"
         << termcolor::reset << endl
         << endl;

    position = GetRandomPosition();
    isHit = GameController::GameController::CheckIsHit(myFleet, position);
    cout << endl;

    std::stringstream ss;
    if (isHit) {
      ss << "(Computer shoot in " << position << " and "
         << "hit your ship !)" << endl;
      printShot(termcolor::red, ss.str(), termcolor::red);
    } else {
      ss << "(Computer shoot in " << position << " and missed )   " << endl;
      printShot(termcolor::color<179, 247, 255>, ss.str(),
                termcolor::color<179, 247, 255>);
    }

    if (CheckIsGameOver(myFleet, enemyFleet)) {
      printMessageWithBorder(termcolor::on_red, "You lose! :(",
                             termcolor::white, 40, '=');
      break;
    }

  } while (true);
}

Position Program::ParsePosition(string input) {
  char cColumn = toupper(input.at(0));
  char cRow = input.at(1);

  int nColumn = (cColumn - 'A');
  Letters lColumn = (Letters)nColumn;

  int nRow = cRow - '0';

  Position outPosition;
  outPosition.Column = lColumn;
  outPosition.Row = nRow;
  return outPosition;
}

Position Program::GetRandomPosition() {
  const int size = 8;
  srand((unsigned int)time(NULL));
  Letters lColumn = (Letters)(rand() % size);
  int nRow = (rand() % size);

  Position position(lColumn, nRow);
  return position;
}

void Program::InitializeGame() {
  InitializeMyFleet();

  InitializeEnemyFleet(enemyFleet);
}

void Program::InitializeMyFleet() {
  myFleet = GameController::GameController::InitializeShips();

  cout << "Please position your fleet (Game board has size from A to H and 1 "
          "to 8) :"
       << endl;

  for (auto &ship : myFleet) {
    while (true) {
      cout << endl;
      cout << "Please enter the start positions for the " << ship.Name
           << " (size: " << ship.Size << ")" << endl;
      string input;
      getline(cin, input);
      Position inputPosition = ParsePosition(input);

      do {
        cout << "Please enter the ship orientation ([V]ertical or "
                "[H]orizontal): "
             << endl;
        ship.Positions = {};
        getline(cin, input);

        if (input == "V" || input == "v") {
          ship.SetPosition(false, inputPosition, ship.Length());
        }

        if (input == "H" || input == "h") {
          ship.SetPosition(true, inputPosition, ship.Length());
        }
      } while (input != "V" && input != "H" && input != "v" && input != "h");

      if (ship.isCollision(myFleet, true)) {
        cout << termcolor::bold << termcolor::on_red
             << "Your ship is overlapping with another, please input the ship "
                "position again."
             << termcolor::reset << endl;
      } else if (ship.isOutOfBounds(8)) {
        cout << termcolor::bold << termcolor::on_red
             << "Your ship is out of bounds of the map, please input the ship "
                "position again."
             << termcolor::reset << endl;
      } else {
        break;
      }
    }
  }
}

void initShip(Ship &ship, int length) {
  ship.Positions = {};
  bool horizontal = random(0, 1);
  int min = random(1, 4);
  int max = random(0, 7);
  if (horizontal) {
    ship.SetPosition(horizontal, Position(Letters(max), min), length);
  } else {
    ship.SetPosition(horizontal, Position(Letters(min), max), length);
  }
}

void generateShips(std::list<Ship> &Fleet) {
  for_each(Fleet.begin(), Fleet.end(), [&Fleet](Ship &ship) {
    if (ship.Name == "Aircraft Carrier") {
      initShip(ship, 5);
    }
    if (ship.Name == "Battleship") {
      do {
        initShip(ship, 4);
      } while (ship.isCollision(Fleet));
    }
    if (ship.Name == "Submarine") {
      do {
        initShip(ship, 3);
      } while (ship.isCollision(Fleet));
    }
    if (ship.Name == "Destroyer") {
      do {
        initShip(ship, 3);
      } while (ship.isCollision(Fleet));
    }
    if (ship.Name == "Patrol Boat") {
      do {
        initShip(ship, 2);
      } while (ship.isCollision(Fleet));
    }
  });
}

void Program::InitializeEnemyFleet(list<Ship> &Fleet) {
  Fleet = GameController::GameController::InitializeShips();
  generateShips(Fleet);
  for (const auto &s : Fleet) {
    std::cout << s.Name << std::endl;
    for (const auto &p : s.Positions) {
      std::cout << p << std::endl;
    }
  }
}
}  // namespace Ascii
}  // namespace Battleship
